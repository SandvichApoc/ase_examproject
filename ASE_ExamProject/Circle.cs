﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ASE_ExamProject
{
    internal class Circle : DrawCommand
    {
        private int radius;
        private String[] split;
        private String peram;

        const int numberOfPeram = 1;
        /// <summary>
        /// Intitialise a new instence of a circle command
        /// </summary>
        /// <param name="c">Canvas this command orders to draw</param>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="peram">The radius of the circle</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        public Circle(Canvas c, Sequence s, String peram, int lineNum) : base(c,s,lineNum)
        {
            this.peram = peram;
        }

        /// <summary>
        /// calculates the radius of the circle from the input peramater
        /// </summary>
        /// <param name="input">The peramater inputted</param>
        private void ParsePeram(String input)
        {
            if (input == null)
            {
                argEX = new System.ApplicationException("Command requires a paramater");
                throw argEX;
            }
            split = input.Split(",");
            if (!(split.Length == numberOfPeram))
            {
                argEX = new System.ApplicationException("Incorrect number of peramaters");
                throw argEX;
            }
            if(!(int.TryParse(split[0], out radius)))
            {
                radius = int.Parse(ParseVar(split[0]));
            }
            if(radius == 0)
            {
                argEX = new System.ApplicationException("Peramater can not be zero");
                throw argEX;
            }
            if (radius < 0)
            {
                argEX = new System.ApplicationException("Peramater is negative");
                throw argEX;
            }
        }
        /// <summary>
        /// Calculates the radius then draws a circle of that radius
        /// </summary>
        public override void run()
        {
            ParsePeram(peram);
            c.DrawCircle(radius);
        }
    }
}
