﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Drawing;

namespace ASE_ExamProject
{
    
    internal class Triangle : DrawCommand
    {
        Point[] points;
        String[] split;
        private int[] intSplit;
        private String peram;
        const int numberOfPeram = 4;
        /// <summary>
        /// Intitialise a new instence of a triangle command
        /// </summary>
        /// <param name="c">Canvas this command orders to draw</param>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="peram">The two other points of the triangle seperated by commas</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        public Triangle(Canvas c, Sequence s, String peram, int lineNum) : base(c,s, lineNum)
        {
            this.peram = peram;
        }
        /// <summary>
        /// Parses the other two points of the triangle
        /// </summary>
        /// <param name="input">The input to parse</param>
        private void ParsePeram(String input)
        {
            if (input == null)
            {
                argEX = new System.ApplicationException("Command requires a peramater");
                throw argEX;
            }

            split = input.Split(',');
            if (!(split.Length == numberOfPeram))
            {
                argEX = new System.ApplicationException("Incorrect number of peramaters");
                throw argEX;
            }
            intSplit = new int[numberOfPeram];
            points = new Point[3];

            //Converts all String integers and variables into integers
            for (int i = 0; i < numberOfPeram; i++)
            {
                if (!(int.TryParse(split[i], out int result)))
                {
                    try
                    {
                        result = int.Parse(ParseVar(split[i]));
                    }
                    catch (ApplicationException e)
                    {
                        throw e;
                    }
                }
                intSplit[i] = result;
            }
            for (int i = 0; i < intSplit.Length; i = i + 2)
            {
                points[i].X = intSplit[i];
                points[i].Y = intSplit[i + 1];
            }
            points[1].X = c.getXPos();
            points[1].Y = c.getYPos();

        }
        /// <summary>
        /// Runs the draw triangle command
        /// </summary>
        public override void run()
        {
            ParsePeram(peram);
            c.DrawTriangle(points);
        }
    }
}
