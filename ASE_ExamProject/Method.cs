﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    public class Method : BodyCommands
    {
        public String methodName { get; set; }
        public int methodPeramNum { get; set; }
        public Sequence methodSequ { get; set; }
        /// <summary>
        /// Intitialise a new instence of a method command command
        /// </summary>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        /// <param name="peram">The name of this method and the peramaters it takes</param>
        public Method(Sequence s, int lineNum, String peram) : base(s, lineNum)
        {
            methodSequ = new Sequence();
            
            String[] split = peram.Split(' ', 2);
            methodName = split[0];
            if (split.Length > 1)
            {

                String[] varSplit = split[1].Split(',');
                for (int i = 0; i < varSplit.Length; i++)
                {
                    varSplit[i] = varSplit[i].Trim().ToLower();
                    if (methodSequ.varList.Exists(variable => variable.varName == varSplit[i]))
                    {
                        argEX = new ApplicationException("Method peramater name already exists");
                        throw argEX;
                    }
                    Variable methodVar = new Variable(varSplit[i], methodSequ, lineNum);
                    methodSequ.varList.Add(methodVar);
                    methodPeramNum++;
                }
            }
        }
        /// <summary>
        /// Runs the method sequence
        /// </summary>
        public override void run()
        {
            try
            {
                methodSequ.MethodRun();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
