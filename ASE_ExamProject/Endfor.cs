﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    public class Endfor : EndCommands
    {
        public Endfor(Sequence s, int lineNum) : base(s,lineNum)
        {
        }
        /// <summary>
        /// When the endfor command is run it iterates the counter of the for it is linked to and then checks if the loop has run for as many times as it's supposed to.
        /// if it hasn't then it gos to the command bellow the loop head as to not run the for command again which would reset the counter
        /// otherwise the sequence continues
        /// </summary>
        public override void run()
        {
            ListPosition = s.forList.FindIndex(For => For.lineNum == startLnum);

            s.forList[ListPosition].itterateCounter();
            if (s.forList[ListPosition].targetIteration > s.forList[ListPosition].loopCounter)
            {
                s.goTo(s.commandSequence.FindIndex(Commands => Commands.lineNum == startLnum));
            }
        }
    }
}
