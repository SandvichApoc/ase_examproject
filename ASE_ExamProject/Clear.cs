﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    public class Clear : DrawCommand
    {
        /// <summary>
        /// Initialises a new clear command
        /// </summary>
        /// <param name="c">Canvas this command orders to draw</param>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        public Clear(Canvas c, Sequence s, int lineNum) : base(c, s, lineNum)
        {
        }
        /// <summary>
        /// Runs the clear command in the linked canvas
        /// </summary>
        public override void run()
        {
            c.clear();
        }
    }
}
