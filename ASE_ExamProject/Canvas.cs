﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using org.mariuszgromada.math.mxparser;

namespace ASE_ExamProject
{
    public class Canvas
    {
        Graphics g;

        Boolean filled;

        public Pen Pen { get; set; }
        public SolidBrush Brush { get; set; }
        public Boolean runCommands { get; set; }

        List<String> colourList = new List<String> {"black", "red", "green", "blue"};
        Color black = Color.Black;
        Color red = Color.Red;
        Color green = Color.Green;
        Color blue = Color.Blue;

        int xPos;
        int yPos;

        const int startPenPos = 0;
        const int defaultPenWidth = 1;

        /// <summary>
        /// Constructor for Canvas. Defaults with a black pen with width 1 at 0,0
        /// </summary>
        /// <param name="g">Graphics context</param>
        public Canvas(Graphics g)
        {
            this.g = g;
            xPos = startPenPos;
            yPos = startPenPos;
            Pen = new Pen(Color.Black, defaultPenWidth);
            Brush = new SolidBrush(black);
            filled = false;
            runCommands = true;
        }

        /// <summary>
        /// Clears the drawing box with the default background colour
        /// TODO: Make the colour selection cleaner with a constant or something
        /// because system.drawing.systemcolors.window is a bit verbose? (I think thats an appropriate word)
        /// </summary>
        public void clear()
        {
                g.Clear(System.Drawing.SystemColors.Window);
        }

        /// <summary>
        /// sets if shapes the canvas draws are filled or not
        /// </summary>
        /// <param name="fill"></param>
        public void setFill(Boolean fill)
        {
            if (runCommands)
            {
                filled = fill;
            }
        }

        /// <summary>
        /// Gets the pen colors this canvas accepts
        /// </summary>
        /// <returns>List of colors this canvas accepts</returns>
        public List<String> getColors()
        {
            return colourList;
        }
        /// <summary>
        /// Get the current X position of the pen
        /// </summary>
        /// <returns>The X position of the pen</returns>
        public int getXPos()
        {
            return xPos;
        }
        /// <summary>
        /// Get the current Y position of the pen
        /// </summary>
        /// <returns>The Y Position of the pen</returns>
        public int getYPos()
        {
            return yPos;
        }

        /// <summary>
        /// Set the pen colour to the inputted colour
        /// </summary>
        /// <param name="colour">The colour to be set to</param>
        public void setPen(String colour)
        {
            if (runCommands)
            {
                if (colour.Equals("black"))
                {
                    Pen.Color = black;
                    Brush.Color = black;
                }
                else if (colour.Equals("red"))
                {
                    Pen.Color = red;
                    Brush.Color = red;
                }
                else if (colour.Equals("green"))
                {
                    Pen.Color = green;
                    Brush.Color = green;
                }
                else if (colour.Equals("blue"))
                {
                    Pen.Color = blue;
                    Brush.Color = blue;
                }
            }
        }

        /// <summary>
        /// Move the pen to the specified position
        /// </summary>
        /// <param name="xEndPos"> the x position the pen will end at</param>
        /// <param name="yEndPos"> the y position the pen will end at</param>
        public void moveto(int xEndPos, int yEndPos)
        {
            if (runCommands)
            {
                xPos = xEndPos;
                yPos = yEndPos;
            }
        }

        /// <summary>
        /// Draw a line from current pen position to the end position entered
        /// </summary>
        /// <param name="xEndPos">x position the line will end at</param>
        /// <param name="yEndPos">y position the line will end at</param>
        public void DrawLine(int xEndPos, int yEndPos)
        {
            if (runCommands)
            {
                g.DrawLine(Pen, xPos, yPos, xEndPos, yEndPos);
                moveto(xEndPos, yEndPos);
            }
        }
        /// <summary>
        /// Draw a rectangle with the x and y sides being different lengths
        /// </summary>
        /// <param name="xLength">Length of the x side</param>
        /// <param name="yLength">Length of the y side</param>
        public void DrawRectangle(int xLength, int yLength)
        {
            if (runCommands)
            {
                if (filled == true)
                {
                    g.FillRectangle(Brush, xPos, yPos, xPos + xLength, yPos + yLength);
                }
                else
                {
                    g.DrawRectangle(Pen, xPos, yPos, xLength, yLength);
                }
            }
            
        }
        /// <summary>
        /// Draw a circle with the current X,Y position as the center
        /// </summary>
        /// <param name="radius">The radius of the circle to be drawn</param>
        public void DrawCircle (int radius)
        {
            if (runCommands)
            {
                if (filled == true)
                {
                    g.FillEllipse(Brush, xPos - radius, yPos - radius, radius * 2, radius * 2);
                }
                else
                {
                    try
                    {
                        g.DrawEllipse(Pen, xPos - radius, yPos - radius, radius * 2, radius * 2);
                    }
                    catch 
                    {
                        throw new ApplicationException("Circle is too big to draw");
                    }
                }
            }
        }
        /// <summary>
        /// Draw a three sided polygon with the current pen position as one of the points
        /// </summary>
        /// <param name="points">XY position of the other two points of the triangle</param>
        public void DrawTriangle (Point[] points)
        {
            if (runCommands)
            {
                if (filled == true)
                {
                    g.FillPolygon(Brush, points);
                }
                else
                {
                    g.DrawPolygon(Pen, points);
                }
            }
        }
        /// <summary>
        /// Reset the pen to it's initial position
        /// </summary>
        public void reset()
        {
            if (runCommands)
            {
                moveto(startPenPos, startPenPos);
            }
        }
    }
}
