﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ASE_ExamProject
{
    public class Rectangle : DrawCommand
    {
        public int xLength { get; set; }
        public int yLength { get; set; }
        private String peram;

        const int numberOfPeram = 2;
        /// <summary>
        /// Intitialise a new instence of a rectangle command
        /// </summary>
        /// <param name="c">Canvas this command orders to draw</param>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="peram">The height and width of the rectangle seperated by a comma</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        public Rectangle(Canvas c, Sequence s, String peram, int lineNum) : base(c,s,lineNum)
        {
            this.peram = peram;
        }
        /// <summary>
        /// Parses the hight and width of the rectangle
        /// </summary>
        /// <param name="input">The height and width conjoined</param>
        private void ParsePeram (String input)
        {
            String[] split;
            int[] intPeram;

            if (input == null)
            {
                argEX = new System.ApplicationException("Command requires a peramater");
                throw argEX;
            }

            split = input.Split(',');

            if (!(split.Length == numberOfPeram))
            {
                argEX = new System.ApplicationException("Incorrect number of peramaters");
                throw argEX;
            }

            intPeram = new int[split.Length];
            for (int i = 0; i < split.Length; i++)
            {
                if (!(int.TryParse(split[i], out int result)))
                {
                    try
                    {
                        result = int.Parse(ParseVar(split[i]));
                    }
                    catch (ApplicationException e)
                    {
                        throw e;
                    }
                }
                intPeram[i] = result;
                if (intPeram[i] <0 )
                {
                    argEX = new System.ApplicationException("Peramater is negative");
                    throw argEX;
                }
            }
            xLength = intPeram[0];
            yLength = intPeram[1];

        }
        /// <summary>
        /// Runs the draw rectangle command
        /// </summary>
        public override void run()
        {
            ParsePeram(peram);
            c.DrawRectangle(xLength, yLength);
        }
    }
}
