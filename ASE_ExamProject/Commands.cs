﻿using System;
using System.Collections.Generic;
using System.Text;
using org.mariuszgromada.math.mxparser;

namespace ASE_ExamProject
{
    public abstract class Commands : Command
    {
        public int lineNum { get; set; }
        protected Sequence s;
        protected Method m;
        protected System.ApplicationException argEX;
        protected List<String> operatorList = new List<String> { "=", "+", "-", "/", "*" };

        public Commands(Sequence s,int lineNum)
        {
            this.s = s;
            this.lineNum = lineNum;
        }
        /// <summary>
        /// Finds the expression of the inputted variable
        /// </summary>
        /// <param name="peram">The name of the variable to be found</param>
        /// <returns>The expression of the variable</returns>
        protected String ParseVar(String peram)
        {
            String parsedVar;
            if (!s.varList.Exists(variable => variable.varName == peram))
            {
                argEX = new System.ApplicationException("Peramater not a number");
                throw argEX;
            }
            if (s.varList.Find(Variable => Variable.varName == peram).exp == null)
            {
                argEX = new System.ApplicationException("Usage of non set variable");
                throw argEX;
            }
            parsedVar = s.varList.Find(Variable => Variable.varName == peram).exp;
            return parsedVar;
        }
        /// <summary>
        /// Performs calculations of the inputted expression
        /// </summary>
        /// <param name="input">The expression to be inputted</param>
        /// <returns>The result of the expression</returns>
        protected String CalcExpression(String input)
        {
            String resultExp;
            Expression e = new Expression();
            String[] split = input.Split(' ');

            for (int i = 0; i < split.Length; i++)
            {
                //Check it is not an operator
                if (!operatorList.Contains(split[i]))
                {
                    //check it is not already a number
                    if (!int.TryParse(split[i], out int result))
                    {
                        split[i] = ParseVar(split[i]);

                    }

                }
            }
            e.setExpressionString(String.Join(" ", split));
            resultExp = Convert.ToInt32(e.calculate()).ToString();
            return resultExp;
        }

        public abstract void run();
    }
}
