﻿using System;
using System.Collections.Generic;
using System.Text;
using org.mariuszgromada.math.mxparser;
using System.Linq;

namespace ASE_ExamProject
{
    public class CalcVar : Commands
    {
        String exp;
        String resultExp;
        String motherVarName;
        private Expression e;
        /// <summary>
        /// Calculates an expression on behalf of it's mother expression
        /// </summary>
        /// <param name="s">The sequence this command is stored in</param>
        /// <param name="varName">The name of the mother variable so it can be found again</param>
        /// <param name="expression">The expression that is calculated when it is called</param>
        public CalcVar(Sequence s, int lineNum, String varName, String expression) : base(s, lineNum)
        {
            exp = expression;
            motherVarName = varName;
        }
        /// <summary>
        /// Finds the operation the expression is then calculates it
        /// </summary>
        private void calcVarExpression()
        {
            e = new Expression();
            String[] split = exp.Split(' ');

            //The set variable operation.
            //If there are more operations than setting it should be simple to add by adding an else if
            if (split[0].Equals("="))
            {
                String input = String.Join(" ", split.Skip(1));
                resultExp = CalcExpression(input);
            }
            else
            {
                argEX = new ApplicationException("variable operation unknown");
                throw argEX;
            }
        }
        /// <summary>
        /// calculate the expression this command was given and update it's parent variable with the result
        /// </summary>
        public override void run()
        {
            calcVarExpression();
            //Finds it's mum and gives it the expression it calculated
            s.varList.Find(Variable => Variable.varName == motherVarName).setExpression(resultExp);
        }
    }
}
