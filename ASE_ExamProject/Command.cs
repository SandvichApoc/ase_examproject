﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    public interface Command
    {
        void run();
    }
}
