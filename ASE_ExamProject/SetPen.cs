﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ASE_ExamProject
{
    internal class SetPen : DrawCommand
    {
        String colour;
        List<String> colourList;
        String[] split;

        const int numberOfPeram = 1;
        /// <summary>
        /// Intitialise a new instence of a set pen command
        /// </summary>
        /// <param name="c">Canvas this command orders to draw</param>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="peram">The colour to set the pen to</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        public SetPen(Canvas c, Sequence s, string peram, int lineNum) : base(c,s,lineNum)
        {
            colourList = c.getColors();
            ParsePeram(peram);
        }
        /// <summary>
        /// Parses the input
        /// </summary>
        /// <param name="input">The input</param>
        private void ParsePeram (String input)
        {
            if (input is null)
            {
                argEX = new System.ApplicationException("Command requires a paramater");
                throw argEX;
            }
            split = input.Split(",");
            if (!(split.Length == numberOfPeram))
            {
                argEX = new System.ApplicationException("Incorrect number of paramaters");
                throw argEX;
            }
            if (!(colourList.Contains(input)))
            {
                argEX = new System.ApplicationException("Peramater not recognised");
                throw argEX;
            }

            colour = input.Trim().ToLower();

        }
        /// <summary>
        /// Runs the set pen command
        /// </summary>
        public override void run()
        {
            c.setPen(colour);
        }
    }
}
