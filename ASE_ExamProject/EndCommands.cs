﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    public abstract class EndCommands : Commands
    {
        public int startLnum { get; set; }
        protected int ListPosition;
        protected EndCommands(Sequence s, int lineNum) : base(s, lineNum)
        {
            startLnum = 0;
        }
    }
}
