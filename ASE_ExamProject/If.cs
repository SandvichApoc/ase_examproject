﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using org.mariuszgromada.math.mxparser;

namespace ASE_ExamProject
{
    public class If : BodyCommands
    {
        private String Statement;
        private readonly List<String> Comparisons = new List<String>() {"==","!=",">","<",">=","<="};
        private readonly List<String> Logical = new List<String>() {"&&","||","!" };
        private List<String> compoundStatement;
        private String statementOperator;
        public Boolean statementResult { get; set; }
        /// <summary>
        /// Intitialise a new instence of a if head command
        /// </summary>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        /// <param name="peram">The statement this if is to calculate</param>
        public If(Sequence s, int lineNum, String peram) : base(s, lineNum)
        {
            Statement = peram.ToLower().Trim();
        }
        /// <summary>
        /// Takes the statement assigned to this command and calculates if it is true or false
        /// </summary>
        private void calcStatement()
        {
            int comparisonCount = 0;
            // compound if statements
            if (Statement.Contains("(") || Statement.Contains(")"))
            {
                // if the statement has brackets but doesn't have any logical operators in it
                if (!Logical.Any(s => Statement.Contains(s)))
                {
                    argEX = new ApplicationException("If statement should not require brackets if it doesn't have a logical operator");
                    throw argEX;
                }
                // do something with this later
                try
                {
                    compoundStatement = parseBrackets(Statement);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                statementOperator = FindCompoundOperator(Statement).Trim();
                String[] split = statementOperator.Split(' ');
                if (split.Length > 1)
                {
                    argEX = new ApplicationException("Statement has multiple top level logical operators, multiple operators should be within brackets");
                    throw argEX;
                }

                if (statementOperator == "!")
                {
                    if (compoundStatement.Count > 1)
                    {
                        argEX = new ApplicationException("To use NOT operator the whole statement needs to be inside brackets");
                        throw argEX;
                    }
                    Not(compoundStatement[0]);

                }
                else if (statementOperator == "||")
                {
                    if (compoundStatement.Count == 1)
                    {
                        argEX = new ApplicationException("Or operator requires two sections in brackets");
                        throw argEX;
                    }
                    if (compoundStatement.Count > 2)
                    {
                        argEX = new ApplicationException("If has too many statements");
                        throw argEX;
                    }
                    Or(compoundStatement);

                }
                else if (statementOperator == "&&")
                {
                    if (compoundStatement.Count == 1)
                    {
                        argEX = new ApplicationException("And operator requires two sections in brackets");
                        throw argEX;
                    }
                    if (compoundStatement.Count > 2)
                    {
                        argEX = new ApplicationException("If has too many statements");
                        throw argEX;
                    }
                    And(compoundStatement);
                }

            }
            // simple if statements
            else
            {
                String[] split = Statement.Split(' ');
                for (int i = 0; i < split.Length; i++)
                {
                    if (Comparisons.Any(s => split[i].Contains(s)))
                    {
                        comparisonCount++;
                    }
                }
                if (comparisonCount > 1)
                {
                    argEX = new ApplicationException("If statement has multiple comparison operators in section");
                    throw argEX;
                }
                else if (comparisonCount == 0)
                {
                    argEX = new ApplicationException("If statement has no comparison operator where there should be");
                }

                if(Statement.Contains("=="))
                {
                    Equal(Statement);
                }
                else if (Statement.Contains("!="))
                {
                    NotEqual(Statement);
                }
                else if (Statement.Contains(">="))
                {
                    BiggerOrEqual(Statement);
                }
                else if (Statement.Contains("<="))
                {
                    LessOrEqual(Statement);
                }
                else if (Statement.Contains("<"))
                {
                    LessThan(Statement);
                }
                else if (Statement.Contains(">"))
                {
                    BiggerThan(Statement);
                }

            }
        }
        /// <summary>
        /// Calculates if the statement this IF was assigned equates to true or false.
        /// If skips the sequence to the end command
        /// </summary>
        public override void run()
        {
            if (endLpos == 0)
            {
                argEX = new System.ApplicationException("IF doesn't have an end");
                throw argEX;
            }
            calcStatement();
            if (!statementResult)
            {
                s.goTo(s.commandSequence.FindIndex(Commands => Commands.lineNum == endLpos));
            }
        }
        /// <summary>
        /// Run for IFs created by an if statement. skips checking for an end or going to the end command
        /// </summary>
        public void subRun()
        {
            calcStatement();
        }
        /// <summary>
        /// Cuts out the if statements between the top level brackets of the given string
        /// </summary>
        /// <param name="input">The string to be processed</param>
        /// <returns>List of the strings between brackets that are not nested</returns>
        private List<String> parseBrackets(String input)
        {
            Stack<int> bracketsPos = new Stack<int>();
            List<String> result = new List<String>();
            for (int i = 0; i < input.Length; i++)
            {
                char tempChar = input[i];
                // if open bracket add it to the list of brackets waiting for a closeing
                if (tempChar == '(')
                {
                    bracketsPos.Push(i);
                }
                // if close brackets check if it is not nested inside another bracket then take that chunk and add it to the list
                else if (tempChar == ')')
                {
                    switch (bracketsPos.Count)
                    {
                        case 0:
                            argEX = new ApplicationException("unexpected closed brackets at character " + i + " of If command");
                            throw argEX;
                        case 1:
                            int start = bracketsPos.Pop();
                            result.Add(input.Substring(start + 1, (i - start) - 1));
                            break;
                        default:
                            bracketsPos.Pop();
                            break;
                    }
                }
            }
            if (bracketsPos.Count != 0)
            {
                argEX = new ApplicationException("Open bracket in If statement does not have close");
                throw argEX;
            }
            return result;
        }
        /// <summary>
        /// Finds the operator that is not within brackets of the statement
        /// </summary>
        /// <param name="input">The statement to be processed</param>
        /// <returns>The top level logical operator</returns>
        private String FindCompoundOperator(String input)
        {
            Stack<int> bracketsPos = new Stack<int>();
            String result = null;
            for (int i = 0; i < input.Length; i++)
            {
                char tempChar = input[i];

                if (tempChar == '(')
                {
                    bracketsPos.Push(i);
                }
                else if (tempChar == ')')
                {
                    bracketsPos.Pop();
                }
                // if the chraracter is not inside a bracket and isn't a bracket add that character to the output
                else if (bracketsPos.Count == 0)
                {
                    result = result + tempChar;
                }
            }

            return result;
        }
        /// <summary>
        /// Calculate if the two sides of the given statement equals each other
        /// </summary>
        /// <param name="statement">The statement to be tested</param>
        private void Equal(String statement)
        {
            int a;
            int b;
            String[] split = statement.Split("==");
            for (int i = 0; i < split.Length; i++)
            {
                split[i] = CalcExpression(split[i].Trim());
            }
            a = Convert.ToInt32(split[0]);
            b = Convert.ToInt32(split[1]);

            if (a == b)
            {
                statementResult = true;
            }
            else
            {
                statementResult = false;
            }
        }
        /// <summary>
        /// Calculate if the two sides of the given statement are not equals each other
        /// </summary>
        /// <param name="statement">The statement to be tested</param>
        private void NotEqual(String statement)
        {
            int a;
            int b;
            String[] split = statement.Split("!=");
            for (int i = 0; i < split.Length; i++)
            {
                split[i] = CalcExpression(split[i].Trim());
            }
            a = Convert.ToInt32(split[0]);
            b = Convert.ToInt32(split[1]);

            if (a == b)
            {
                statementResult = false;
            }
            else
            {
                statementResult = true;
            }
        }
        /// <summary>
        /// Calculate if the first side of the given statement is bigger than the seccond
        /// </summary>
        /// <param name="statement">The statement to be tested</param>
        private void BiggerThan(String statement)
        {
            int a;
            int b;
            String[] split = statement.Split(">");
            for (int i = 0; i < split.Length; i++)
            {
                split[i] = CalcExpression(split[i].Trim());
            }
            a = Convert.ToInt32(split[0]);
            b = Convert.ToInt32(split[1]);

            if (a > b)
            {
                statementResult = true;
            }
            else
            {
                statementResult = false;
            }
        }
        /// <summary>
        /// calculate if the first side of the given statement is smaller than the seccond
        /// </summary>
        /// <param name="statement">The statement to be tested</param>
        private void LessThan(String statement)
        {
            int a;
            int b;
            String[] split = statement.Split("<");
            for (int i = 0; i < split.Length; i++)
            {
                split[i] = CalcExpression(split[i].Trim());
            }
            a = Convert.ToInt32(split[0]);
            b = Convert.ToInt32(split[1]);

            if (a < b)
            {
                statementResult = true;
            }
            else
            {
                statementResult = false;
            }
        }
        /// <summary>
        /// calculate if the first side of the given statement is bigger or equal to the seccond
        /// </summary>
        /// <param name="statement">The statement to be tested</param>
        private void BiggerOrEqual(String statement)
        {
            int a;
            int b;
            String[] split = statement.Split(">=");
            for (int i = 0; i < split.Length; i++)
            {
                split[i] = CalcExpression(split[i].Trim());
            }
            a = Convert.ToInt32(split[0]);
            b = Convert.ToInt32(split[1]);

            if (a >= b)
            {
                statementResult = true;
            }
            else
            {
                statementResult = false;
            }
        }
        /// <summary>
        /// calculate if the first side of the given statement is smaller or equal to the seccond
        /// </summary>
        /// <param name="statement"></param>
        private void LessOrEqual(String statement)
        {
            int a;
            int b;
            String[] split = statement.Split("<=");
            for (int i = 0; i < split.Length; i++)
            {
                split[i] = CalcExpression(split[i].Trim());
            }
            a = Convert.ToInt32(split[0]);
            b = Convert.ToInt32(split[1]);

            if (a <= b)
            {
                statementResult = true;
            }
            else
            {
                statementResult = false;
            }
        }
        /// <summary>
        /// Reverses the result of the sub if statement
        /// </summary>
        /// <param name="Statement">The statement to be tested</param>
        private void Not(String Statement)
        {
            If subIf = new If(s, 0, Statement);
            subIf.subRun();
            if(subIf.statementResult)
            {
                statementResult = false;
            }
            else
            {
                statementResult = true;
            }

        }
        /// <summary>
        /// calculate if either sub if results to true
        /// </summary>
        /// <param name="statements">The statement to be tested</param>
        private void Or(List<String> statements)
        {
            If subA = new If(s, 0, statements[0]);
            If subB = new If(s, 0, statements[1]);
            subA.subRun();
            subB.subRun();

            if (subA.statementResult || subB.statementResult)
            {
                statementResult = true;
            }
            else
            {
                statementResult = false;
            }

        }
        /// <summary>
        /// calculates if both sub if results to true
        /// </summary>
        /// <param name="statements">The statement to be tested</param>
        private void And(List<String> statements)
        {
            If subA = new If(s, 0, statements[0]);
            If subB = new If(s, 0, statements[1]);
            subA.subRun();
            subB.subRun();

            if (subA.statementResult && subB.statementResult)
            {
                statementResult = true;
            }
            else
            {
                statementResult = false;
            }
        }
    }
}
