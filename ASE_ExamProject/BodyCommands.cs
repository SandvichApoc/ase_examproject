﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    public abstract class BodyCommands : Commands
    {
        public int endLpos { get; set; }
        protected BodyCommands(Sequence s, int lineNum) : base(s, lineNum)
        {
            endLpos = 0;
        }
    }
}
