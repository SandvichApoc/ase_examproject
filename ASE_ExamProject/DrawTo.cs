﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ASE_ExamProject
{
    public class DrawTo : DrawCommand
    {
        public int xEndPos { get; set; }
        public int yEndPos { get; set; }
        private String peram;

        const int numOfPeram = 2;
        /// <summary>
        /// Initialises a new instence of a drawto command
        /// </summary>
        /// <param name="c">Canvas this command orders to draw</param>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="peram">The X/Y coardinates to draw from the current position to</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        public DrawTo(Canvas c, Sequence s, String peram, int lineNum) : base(c,s, lineNum)
        {
            this.peram = peram;
        }

        /// <summary>
        /// Parse the raw parameter to be used as input
        /// </summary>
        /// <param name="input">Input parameters to be processed</param>
        private void ParsePeram(String input)
        {
            String[] split;
            int[] intPeram;
            if (input == null)
            {
                argEX = new System.ApplicationException("Command requires a peramater");
                throw argEX;
            }
            split = input.Split(',');
            intPeram = new int[split.Length];
            if (!(split.Length == numOfPeram))
            {
                argEX = new System.ApplicationException("Incorrect number of peramaters");
                throw argEX;
            }
            for (int i = 0; i < split.Length; i++)
            {
                if (!(int.TryParse(split[i], out int result)))
                {
                    try
                    {
                        result = int.Parse(ParseVar(split[i]));
                    }
                    catch (ApplicationException e)
                    {
                        throw e;
                    }
                }
                intPeram[i] = result;
                if (intPeram[i] < 0)
                {
                    argEX = new System.ApplicationException("Peramater is negative");
                    throw argEX;
                }
            }
            xEndPos = intPeram[0];
            yEndPos = intPeram[1];

        }
        /// <summary>
        /// Calculates the end position then draws to that position
        /// </summary>
        public override void run()
        {
            ParsePeram(peram);
            c.DrawLine(xEndPos, yEndPos);
        }
    }
}
