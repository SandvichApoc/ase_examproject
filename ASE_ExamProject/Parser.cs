﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ASE_ExamProject
{
    public class Parser
    {
        protected Canvas c;
        protected Sequence s;

        private int lineNum;
        public String command { get; set; }
        public String peram { get; set; }
        private Stack<BodyCommands> endWaitingList;
        private Method cachedMethod;
        private Boolean methodFlag = false;

        /// <summary>
        /// Intitialise a new instence of a parser
        /// </summary>
        /// <param name="c">The canvas all commands created by this parser drawto</param>
        /// <param name="s">The sequence of commands all commands created by this parser are added to</param>
        public Parser(Canvas c, Sequence s)
        {
            this.c = c;
            this.s = s;
        }
        /// <summary>
        /// Parse the code into a series of strings that can be parsed by the command parser
        /// </summary>
        /// <param name="inputCode">The code to be parsed into command lines</param>
        public void ParseCode(String inputCode)
        {
            Clear();
            String[] commandLines;
            String inputCommand;
            endWaitingList = new Stack<BodyCommands>();

            commandLines = inputCode.Split("\n");
            for (int i = 0; i < commandLines.Length; i++)
            {
                inputCommand = commandLines[i];
                lineNum = i + 1;
                ParseCommand(inputCommand);
            }
            if (methodFlag)
            {
                s.errorlist = s.errorlist + "Line " + cachedMethod.lineNum + ": Method is not closed\n";
                s.fatalErr = true;
            }
        }
        /// <summary>
        /// Parse a given command into the command and it's peram 
        /// </summary>
        /// <param name="inputCommand">The Command to be parsed</param>
        public void ParseCommand(string inputCommand)
        {
            //clean up input then split it into command and peram
            inputCommand = inputCommand.Trim().ToLower();
            String[] split = inputCommand.Split(' ', 2);
            command = split[0];
            peram = null;
            if (split.Length >= 2)
            {
                peram = split[1];
            }

            //identify and execute the typed command
            if (command.Equals("moveto"))
            {
                if (methodFlag)
                {
                    MoveTo moveToCommand = new MoveTo(c, cachedMethod.methodSequ, peram, lineNum);
                    cachedMethod.methodSequ.commandSequence.Add(moveToCommand);
                }
                else
                {
                    MoveTo moveToCommand = new MoveTo(c, s, peram, lineNum);
                    s.commandSequence.Add(moveToCommand);
                }

            }
            else if (command.Equals("drawto"))
            {
                if (methodFlag)
                {
                    DrawTo drawToCommand = new DrawTo(c, cachedMethod.methodSequ, peram, lineNum);
                    cachedMethod.methodSequ.commandSequence.Add(drawToCommand);
                }
                else
                {
                    DrawTo drawToCommand = new DrawTo(c, s, peram, lineNum);
                    s.commandSequence.Add(drawToCommand);
                }
            }
            else if (command.Equals("clear"))
            {
                Clear clearCommand = new Clear(c, s, lineNum);
                s.commandSequence.Add(clearCommand);

            }
            else if (command.Equals("reset"))
            {
                if (methodFlag)
                {
                    Reset resetCommand = new Reset(c, cachedMethod.methodSequ, lineNum);
                    cachedMethod.methodSequ.commandSequence.Add(resetCommand);
                }
                else
                {
                    Reset resetCommand = new Reset(c, s, lineNum);
                    s.commandSequence.Add(resetCommand);
                }
            }
            else if (command.Equals("pen"))
            {

                if (methodFlag)
                {
                    SetPen penCommand = new SetPen(c, cachedMethod.methodSequ, peram, lineNum);
                    cachedMethod.methodSequ.commandSequence.Add(penCommand);
                }
                else
                {
                    SetPen penCommand = new SetPen(c, s, peram, lineNum);
                    s.commandSequence.Add(penCommand);
                }
            }
            else if (command.Equals("fill"))
            {
                if (methodFlag)
                {
                    Fill fillCommand = new Fill(c, cachedMethod.methodSequ, peram, lineNum);
                    cachedMethod.methodSequ.commandSequence.Add(fillCommand);
                }
                else
                {
                    Fill fillCommand = new Fill(c, s, peram, lineNum);
                    s.commandSequence.Add(fillCommand);
                }
            }
            else if (command.Equals("rectangle"))
            {

                if (methodFlag)
                {
                    Rectangle rectCommand = new Rectangle(c, cachedMethod.methodSequ, peram, lineNum);
                    cachedMethod.methodSequ.commandSequence.Add(rectCommand);
                }
                else
                {
                    Rectangle rectCommand = new Rectangle(c, s, peram, lineNum);
                    s.commandSequence.Add(rectCommand);
                }
            }
            else if (command.Equals("circle"))
            {

                if (methodFlag)
                {
                    Circle circleCommand = new Circle(c, cachedMethod.methodSequ, peram, lineNum);
                    cachedMethod.methodSequ.commandSequence.Add(circleCommand);
                }
                else
                {
                    Circle circleCommand = new Circle(c, s, peram, lineNum);
                    s.commandSequence.Add(circleCommand);
                }
            }
            else if (command.Equals("triangle"))
            {
                if (methodFlag)
                {
                    Triangle triCommand = new Triangle(c, cachedMethod.methodSequ, peram, lineNum);
                    cachedMethod.methodSequ.commandSequence.Add(triCommand);
                }
                else
                {
                    Triangle triCommand = new Triangle(c, s, peram, lineNum);
                    s.commandSequence.Add(triCommand);
                }

            }
            else if (command.Equals("var"))
            {
                if (methodFlag)
                {
                    Variable var = new Variable(peram, cachedMethod.methodSequ, lineNum);
                    cachedMethod.methodSequ.varList.Add(var);
                }
                else
                {
                    Variable var = new Variable(peram, s, lineNum);
                    s.varList.Add(var);
                }

            }
            else if (s.varList.Exists(variable => variable.varName == command))
            {
                    CalcVar calCommand = new CalcVar(s, lineNum, command, peram);
                    s.commandSequence.Add(calCommand);

            }
            else if (methodFlag && cachedMethod.methodSequ.varList.Exists(variable => variable.varName == command))
            {
                CalcVar calCommand = new CalcVar(cachedMethod.methodSequ, lineNum, command, peram);
                cachedMethod.methodSequ.commandSequence.Add(calCommand);
            }
            else if (command.Equals("for"))
            {
                if (methodFlag)
                {
                    For forCommand = new For(cachedMethod.methodSequ, lineNum, peram);
                    cachedMethod.methodSequ.commandSequence.Add(forCommand);
                    cachedMethod.methodSequ.forList.Add(forCommand);
                    endWaitingList.Push(forCommand);
                }
                else
                {
                    For forCommand = new For(s, lineNum, peram);
                    s.commandSequence.Add(forCommand);
                    s.forList.Add(forCommand);
                    endWaitingList.Push(forCommand);
                }
            }
            else if (command.Equals("endfor"))
            {
                if (endWaitingList.Peek() is For)
                {
                    if (methodFlag)
                    {
                        Endfor endForCommand = new Endfor(cachedMethod.methodSequ, lineNum);
                        endWaitingList.Peek().endLpos = lineNum;
                        endForCommand.startLnum = endWaitingList.Pop().lineNum;
                        cachedMethod.methodSequ.commandSequence.Add(endForCommand);
                    }
                    else
                    {
                        Endfor endForCommand = new Endfor(s, lineNum);
                        endWaitingList.Peek().endLpos = lineNum;
                        endForCommand.startLnum = endWaitingList.Pop().lineNum;
                        s.commandSequence.Add(endForCommand);
                    }
                }
                else
                {
                    s.errorlist = s.errorlist + "Line " + lineNum + ": Another command must have an end before this one\n";
                    s.fatalErr = true;
                }
            }
            else if (command.Equals("if"))
            {

                if (methodFlag)
                {
                    If ifCommand = new If(cachedMethod.methodSequ, lineNum, peram);
                    cachedMethod.methodSequ.commandSequence.Add(ifCommand);
                    endWaitingList.Push(ifCommand);
                }
                else
                {
                    If ifCommand = new If(s, lineNum, peram);
                    s.commandSequence.Add(ifCommand);
                    endWaitingList.Push(ifCommand);
                }

            }
            else if (command.Equals("endif"))
            {
                if (endWaitingList.Peek() is If)
                {

                    if (methodFlag)
                    {
                        EndIf endIfCommand = new EndIf(cachedMethod.methodSequ, lineNum);
                        endWaitingList.Peek().endLpos = lineNum;
                        endIfCommand.startLnum = endWaitingList.Pop().lineNum;
                        cachedMethod.methodSequ.commandSequence.Add(endIfCommand);
                    }
                    else
                    {
                        EndIf endIfCommand = new EndIf(s, lineNum);
                        endWaitingList.Peek().endLpos = lineNum;
                        endIfCommand.startLnum = endWaitingList.Pop().lineNum;
                        s.commandSequence.Add(endIfCommand);
                    }
                }
                else
                {
                    s.errorlist = s.errorlist + "Line " + lineNum + ": Another command must have an end before this one\n";
                    s.fatalErr = true;
                }
            }
            else if (command.Equals("method"))
            {
                methodFlag = true;
                Method methodHead = new Method(s, lineNum, peram);
                cachedMethod = methodHead;
                s.methodList.Add(methodHead);
            }
            else if (command.Equals("endmethod"))
            {
                if(endWaitingList.Count != 0)
                {
                    s.errorlist = s.errorlist + "Line " + lineNum + ": Method can not be closed untill all body commands are closed\n";
                    s.fatalErr = true;
                }
                methodFlag = false;
                cachedMethod = null;
            }
            else if (s.methodList.Exists(method => method.methodName == command))
            {
                RunMethod runMcommand = new RunMethod(s, lineNum, command, peram);
                s.commandSequence.Add(runMcommand);
            }
            else if (!command.Equals(""))
            {
                s.errorlist = s.errorlist + "Line " + lineNum + ": Command not recognised\n";
            }
        }
        /// <summary>
        /// reset things from the previous execution
        /// </summary>
        private void Clear()
        {
            methodFlag = false;
            s.errorlist = null;
            s.fatalErr = false;
            s.commandSequence.Clear();
            s.varList.Clear();
            s.forList.Clear();
            s.methodList.Clear();
            c.clear();
            c.reset();
            c.setPen("black");
            c.setFill(false);
        }
    }
}
