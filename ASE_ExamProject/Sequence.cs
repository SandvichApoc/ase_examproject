﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    public class Sequence
    {
        public String errorlist { get; set; }
        public List<Variable> varList { get; set; }
        public List<Method> methodList { get; set; }
        public List<For> forList { get; set; }
        public List<Commands> commandSequence { get; set; }
        public Boolean fatalErr { get; set; }
        private int i;
        /// <summary>
        /// Intitialise a new instence of a seqeunce
        /// </summary>
        public Sequence()
        {
            commandSequence = new List<Commands>();
            varList = new List<Variable>();
            forList = new List<For>();
            methodList = new List<Method>();
        }
        /// <summary>
        /// Loops through the sequence of commands running each one
        /// </summary>
        public void runSequence()
        {
            if (fatalErr == false)
            {
                for (i = 0; i < commandSequence.Count; i++)
                {
                    try
                    {
                        commandSequence[i].run();
                    }
                    catch (ApplicationException e)
                    {
                        errorlist = errorlist + "Line " + commandSequence[i].lineNum + ": " + e.Message + "\n";
                    }

                }
            }
            
        }
        /// <summary>
        /// Run method to be used by user generated methods. that throws the exceptions to the main sequence for unified error reporting.
        /// </summary>
        public void MethodRun()
        {
            for (i = 0; i < commandSequence.Count; i++)
            {
                try
                {
                    commandSequence[i].run();
                }
                catch (ApplicationException e)
                {
                    throw e;
                }
            }
        }
        /// <summary>
        /// Sets the sequence index to the specified location
        /// </summary>
        /// <param name="sequLocation">The number in the sequence to go to</param>
        public void goTo(int sequLocation)
        {
            i = sequLocation;
        }
    }
}
