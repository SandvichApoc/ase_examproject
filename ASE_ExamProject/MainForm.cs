﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_ExamProject
{
    public partial class MainForm : Form
    {
        const int xBitmap = 400;
        const int yBitmap = 400;

        Bitmap DrawBitmap = new Bitmap(xBitmap,yBitmap);
        Canvas DrawCanvas;
        Parser FormParser;
        Sequence ProgramSequence;

        public MainForm()
        {
            InitializeComponent();
            DrawCanvas = new Canvas(Graphics.FromImage(DrawBitmap));
            ProgramSequence = new Sequence();
            FormParser = new Parser(DrawCanvas, ProgramSequence);
        }

        private void DrawBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImageUnscaled(DrawBitmap, 0,0);// draw the bitmap onto drawbox
        }
        //When command button is clicked parse what is in the command box
        private void CommBtn_MouseClick(object sender, MouseEventArgs e)
        {
            Syntax_box.Text = "";
            DrawCanvas.runCommands = true;
            FormParser.ParseCommand(CommBox.Text);
            Syntax_box.Text = ProgramSequence.errorlist;
            Refresh();
        }
        //When the code button is clicked parse each line in the code box
        private void CodeBtn_MouseClick(object sender, MouseEventArgs e)
        {
            Syntax_box.Text = "";
            DrawCanvas.runCommands = true;
            FormParser.ParseCode(CodeBox.Text);
            ProgramSequence.runSequence();
            Syntax_box.Text = ProgramSequence.errorlist;
            Refresh();
        }
        //Parse the commands in the code box but do not execute them
        private void SyntaxBtn_MouseClick(object sender, MouseEventArgs e)
        {
            DrawCanvas.runCommands = false;
            Syntax_box.Text = "";
            FormParser.ParseCode(CodeBox.Text);
            ProgramSequence.runSequence();
            Syntax_box.Text = ProgramSequence.errorlist;
            Refresh();
        }

        private void Load_btn_MouseClick(object sender, MouseEventArgs e)
        {
            OpenFileDialog loadfile = new OpenFileDialog();
            if (loadfile.ShowDialog() == DialogResult.OK)
            {
                CodeBox.Text = File.ReadAllText(loadfile.FileName);
            }
        }

        private void Save_btn_MouseClick(object sender, MouseEventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.DefaultExt = "txt";
            if (savefile.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(savefile.FileName, CodeBox.Text);
            }
        }
    }
}
