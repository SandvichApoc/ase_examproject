﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ASE_ExamProject
{
    public class MoveTo : DrawCommand
    {
        public int xEndPos { get; set; }
        public int yEndPos { get; set; }
        private String peram;

        const int numberOfPeram = 2;
        /// <summary>
        /// Intitialise a new instence of a moveto command
        /// </summary>
        /// <param name="c">Canvas this command orders to draw</param>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="peram">The X/Y coardinates to move to seperates by a comma</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        public MoveTo(Canvas c,Sequence s, string peram, int lineNum) : base(c,s,lineNum)
        {
            this.peram = peram;
        }

        /// <summary>
        /// Parse the raw parameter to be used as input
        /// </summary>
        /// <param name="input">Input parameters to be processed</param>
        private void ParsePeram(String input)
        {
            String[] split;
            int[] intPeram;

            if (input == null)
            {
                argEX = new System.ApplicationException("Command requires a peramater");
                throw argEX;
            }
            split = input.Split(',');

            if (!(split.Length == numberOfPeram))
            {
                argEX = new System.ApplicationException("Incorrect number of peramaters");
                throw argEX;
            }

            intPeram = new int[split.Length];
            for (int i = 0; i < split.Length; i++)
            {
                if (!(int.TryParse(split[i], out int result)))
                {
                    try
                    {
                        result = int.Parse(ParseVar(split[i]));
                    }
                    catch (ApplicationException e)
                    {
                        throw e;
                    }
                }
                intPeram[i] = result;
                if (intPeram[i] < 0)
                {
                    argEX = new System.ApplicationException("Peramater is negative");
                    throw argEX;
                }
            }
            xEndPos = intPeram[0];
            yEndPos = intPeram[1];

        }
        /// <summary>
        /// calculate the X/Y position then move the cursor to that position
        /// </summary>
        public override void run()
        {
            ParsePeram(peram);
            c.moveto(xEndPos, yEndPos);
        }
    }
}
