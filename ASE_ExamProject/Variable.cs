﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ASE_ExamProject
{
    public class Variable
    {
        public String varName { get; set; }
        public String exp { get; set; }
        private Sequence s;
        public int lineNum { get; set; }

        /// <summary>
        /// Intitialise a new instence of a variable
        /// </summary>
        /// <param name="varName">What the variable is called</param>
        public Variable(String varName, Sequence s, int lineNum)
        {
            this.s = s;
            this.lineNum = lineNum;
            varName.Trim().ToLower();
            this.varName = varName;
        }   
        /// <summary>
        /// Sets the expression of the variable
        /// </summary>
        /// <param name="input">What the variable expression is to be set to</param>
        public void setExpression(String input)
        {
            exp = input;
        }
    }
}
