﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    public abstract class DrawCommand : Commands
    {
        protected Canvas c;
        /// <summary>
        /// Constructor for commands that draw onto the canvas
        /// </summary>
        /// <param name="c">The canvas that will be drawn onto</param>
        public DrawCommand(Canvas c, Sequence s, int lineNum) : base(s,lineNum)
        {
            this.c = c;
        }
    }
}
