﻿
namespace ASE_ExamProject
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CodeBox = new System.Windows.Forms.RichTextBox();
            this.CommBox = new System.Windows.Forms.TextBox();
            this.CommBtn = new System.Windows.Forms.Button();
            this.CodeBtn = new System.Windows.Forms.Button();
            this.DrawBox = new System.Windows.Forms.PictureBox();
            this.SyntaxBtn = new System.Windows.Forms.Button();
            this.Syntax_box = new System.Windows.Forms.RichTextBox();
            this.Load_btn = new System.Windows.Forms.Button();
            this.Save_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DrawBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CodeBox
            // 
            this.CodeBox.Location = new System.Drawing.Point(12, 12);
            this.CodeBox.Name = "CodeBox";
            this.CodeBox.Size = new System.Drawing.Size(379, 171);
            this.CodeBox.TabIndex = 0;
            this.CodeBox.Text = "";
            // 
            // CommBox
            // 
            this.CommBox.Location = new System.Drawing.Point(12, 189);
            this.CommBox.Name = "CommBox";
            this.CommBox.Size = new System.Drawing.Size(379, 23);
            this.CommBox.TabIndex = 1;
            // 
            // CommBtn
            // 
            this.CommBtn.Location = new System.Drawing.Point(12, 218);
            this.CommBtn.Name = "CommBtn";
            this.CommBtn.Size = new System.Drawing.Size(140, 23);
            this.CommBtn.TabIndex = 2;
            this.CommBtn.Text = "Execute Command";
            this.CommBtn.UseVisualStyleBackColor = true;
            this.CommBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CommBtn_MouseClick);
            // 
            // CodeBtn
            // 
            this.CodeBtn.Location = new System.Drawing.Point(158, 218);
            this.CodeBtn.Name = "CodeBtn";
            this.CodeBtn.Size = new System.Drawing.Size(112, 23);
            this.CodeBtn.TabIndex = 3;
            this.CodeBtn.Text = "Execute Code";
            this.CodeBtn.UseVisualStyleBackColor = true;
            this.CodeBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CodeBtn_MouseClick);
            // 
            // DrawBox
            // 
            this.DrawBox.BackColor = System.Drawing.SystemColors.Window;
            this.DrawBox.Location = new System.Drawing.Point(397, 12);
            this.DrawBox.Name = "DrawBox";
            this.DrawBox.Size = new System.Drawing.Size(400, 400);
            this.DrawBox.TabIndex = 4;
            this.DrawBox.TabStop = false;
            this.DrawBox.Paint += new System.Windows.Forms.PaintEventHandler(this.DrawBox_Paint);
            // 
            // SyntaxBtn
            // 
            this.SyntaxBtn.Location = new System.Drawing.Point(276, 218);
            this.SyntaxBtn.Name = "SyntaxBtn";
            this.SyntaxBtn.Size = new System.Drawing.Size(115, 23);
            this.SyntaxBtn.TabIndex = 5;
            this.SyntaxBtn.Text = "Check syntax";
            this.SyntaxBtn.UseVisualStyleBackColor = true;
            this.SyntaxBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SyntaxBtn_MouseClick);
            // 
            // Syntax_box
            // 
            this.Syntax_box.Location = new System.Drawing.Point(12, 273);
            this.Syntax_box.Name = "Syntax_box";
            this.Syntax_box.ReadOnly = true;
            this.Syntax_box.Size = new System.Drawing.Size(379, 139);
            this.Syntax_box.TabIndex = 6;
            this.Syntax_box.Text = "Syntax errors go here";
            // 
            // Load_btn
            // 
            this.Load_btn.Location = new System.Drawing.Point(12, 244);
            this.Load_btn.Name = "Load_btn";
            this.Load_btn.Size = new System.Drawing.Size(70, 23);
            this.Load_btn.TabIndex = 7;
            this.Load_btn.Text = "Load";
            this.Load_btn.UseVisualStyleBackColor = true;
            this.Load_btn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Load_btn_MouseClick);
            // 
            // Save_btn
            // 
            this.Save_btn.Location = new System.Drawing.Point(82, 244);
            this.Save_btn.Name = "Save_btn";
            this.Save_btn.Size = new System.Drawing.Size(70, 23);
            this.Save_btn.TabIndex = 8;
            this.Save_btn.Text = "Save";
            this.Save_btn.UseVisualStyleBackColor = true;
            this.Save_btn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Save_btn_MouseClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Save_btn);
            this.Controls.Add(this.Load_btn);
            this.Controls.Add(this.Syntax_box);
            this.Controls.Add(this.SyntaxBtn);
            this.Controls.Add(this.DrawBox);
            this.Controls.Add(this.CodeBtn);
            this.Controls.Add(this.CommBtn);
            this.Controls.Add(this.CommBox);
            this.Controls.Add(this.CodeBox);
            this.Name = "MainForm";
            this.Text = "ASE Project From";
            ((System.ComponentModel.ISupportInitialize)(this.DrawBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox CodeBox;
        private System.Windows.Forms.TextBox CommBox;
        private System.Windows.Forms.Button CommBtn;
        private System.Windows.Forms.Button CodeBtn;
        private System.Windows.Forms.PictureBox DrawBox;
        private System.Windows.Forms.Button SyntaxBtn;
        private System.Windows.Forms.RichTextBox Syntax_box;
        private System.Windows.Forms.Button Load_btn;
        private System.Windows.Forms.Button Save_btn;
    }
}

