﻿using System;
using System.Collections.Generic;
using System.Text;
using org.mariuszgromada.math.mxparser;

namespace ASE_ExamProject
{
    public class For : BodyCommands
    {
        private string peram;
        public int targetIteration { get; set; }
        public int loopCounter { get; set; }
        /// <summary>
        /// Intitialise a new instence of a for head command
        /// </summary>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        /// <param name="peram">The expression of how many itterations this loop is to continue for</param>
        public For(Sequence s, int lineNum, String peram) : base(s,lineNum)
        {
            this.peram = peram;
        }

        /// <summary>
        /// Adds one to the loop counter when the endloop is reached
        /// </summary>
        public void itterateCounter()
        {
            loopCounter++;
        }
        /// <summary>
        /// Calculates the number of times for the loop to iterate
        /// </summary>
        /// <param name="peram">The origional expression to calculate</param>
        /// <returns>The result of the expression</returns>
        private String CalcForExpression (String peram)
        {
            if (peram == null)
            {
                argEX = new ApplicationException("For loop peramater is missing");
                throw argEX;
            }

            String resultExp = CalcExpression(peram);
            return resultExp;
 
        }
        /// <summary>
        /// Set the number of times to itterate to the result of the expression
        /// </summary>
        public override void run()
        {
            if (endLpos == 0)
            {
                argEX = new System.ApplicationException("for loop doesn't have an end");
                throw argEX;
            }
            loopCounter = 0;
            targetIteration = Convert.ToInt32(CalcForExpression(peram));
        }
    }
}
