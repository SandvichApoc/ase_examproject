﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    internal class Reset : DrawCommand
    {
        /// <summary>
        /// Intitialise a new instence of a reset command
        /// </summary>
        /// <param name="c">Canvas this command orders to draw</param>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        public Reset(Canvas c, Sequence s, int lineNum) : base(c,s,lineNum)
        {
            this.lineNum = lineNum;
        }
        /// <summary>
        /// Runs the reset command
        /// </summary>
        public override void run()
        {
            c.reset();
        }
    }
}
