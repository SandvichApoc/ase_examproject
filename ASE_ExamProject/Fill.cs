﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ASE_ExamProject
{
    internal class Fill : DrawCommand
    {
        private Boolean fill;
        private String[] split;

        const int numberOfPeram = 1;
        /// <summary>
        /// Intitialise a new instence of a fill command
        /// </summary>
        /// <param name="c">Canvas this command orders to draw</param>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="peram">String that sets the fill to be on or off</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        public Fill(Canvas c, Sequence s, String peram, int lineNum) : base(c,s,lineNum)
        {
            ParsePeram(peram);
        }
        /// <summary>
        /// Parses the input to determin if it is to be set on or off
        /// </summary>
        /// <param name="input">the user inputted peramater</param>
        private void ParsePeram(String input)
        {
            if (input == null)
            {
                argEX = new System.ApplicationException("Command requires a peramater");
                throw argEX;
            }
            split = input.Split(",");
            if (!(split.Length == numberOfPeram))
            {
                argEX = new System.ApplicationException("Incorrect number of paramaters");
                throw argEX;
            }
            input = input.Trim().ToLower();
            if (input.Equals("on"))
            {
                fill = true;
            }else if (input.Equals("off"))
            {
                fill = false;
            }else
            {
                argEX = new System.ApplicationException("Incorrect peramater");
                throw argEX;
            }

        }
        /// <summary>
        /// Sets the canvas fill to be on or off
        /// </summary>
        public override void run()
        {
            c.setFill(fill);
        }
    }
}
