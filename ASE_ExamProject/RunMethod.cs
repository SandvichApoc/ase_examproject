﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASE_ExamProject
{
    public class RunMethod : Commands
    {
        private String MethodName;
        private int methodPeramNum;
        private Sequence methodSequence;
        private String peram;
        /// <summary>
        /// Intitialise a new instence of a run method command
        /// </summary>
        /// <param name="s">Sequence of commands this command is in</param>
        /// <param name="lineNum">The line number of this command from the origional string</param>
        /// <param name="MethodName">The name of the method to run</param>
        /// <param name="peram">The peramaters to run the method with</param>
        public RunMethod(Sequence s, int lineNum, String MethodName, String peram) : base(s, lineNum)
        {
            this.MethodName = MethodName;
            methodSequence = s.methodList.Find(method => method.methodName == MethodName).methodSequ;
            methodPeramNum = s.methodList.Find(method => method.methodName == MethodName).methodPeramNum;
            this.peram = peram;
        }
        /// <summary>
        /// Runs the assigned method
        /// </summary>
        public override void run()
        {
            if (!(peram == null))
            {
                String[] split = peram.Split(',');
                if (!(split.Length == methodPeramNum))
                {
                    argEX = new ApplicationException("Method Call has incorect number of peramaters");
                    throw argEX;
                }

                for (int i = 0; i < split.Length; i++)
                {
                    split[i] = split[i].Trim().ToLower();
                    methodSequence.varList[i].exp = CalcExpression(split[i]);
                }
            }

            methodSequence.MethodRun();
        }

    }
}
