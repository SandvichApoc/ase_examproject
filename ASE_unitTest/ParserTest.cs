using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_ExamProject;
using System.Drawing;

namespace ASE_unitTest
{
    [TestClass]
    public class ParserTest
    {
        Bitmap testBitmap = new Bitmap(400, 400);
        Canvas testCanvas;
        Sequence testSeq = new Sequence();
        Parser testParser;
        /// <summary>
        /// Test that command can be separated from the peramater and formatted correctly
        /// </summary>
        [TestMethod]
        public void Parser_TestSeperateCommand()
        {
            //Arrange
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));
            testParser = new Parser(testCanvas, testSeq);
            string expected = "moveto";

            //Act
            testParser.ParseCommand("MovETo 200,200");
            string actual = testParser.command;

            //Assert
            Assert.AreEqual(expected, actual);

        }
        /// <summary>
        /// Test to see if the peramater is successfully split from the command
        /// </summary>
        [TestMethod]
        public void Parser_TestSeperatePeramater()
        {
            //Arrange
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));
            testParser = new Parser(testCanvas, testSeq);
            string expected = "200,200";

            //Act
            testParser.ParseCommand("command 200,200");
            string actual = testParser.peram;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test that if a invalid command is entered it is logged in the error list
        /// </summary>
        [TestMethod]
        public void Parser_TestInvalidCommand()
        {
            //Arrange
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));
            testParser = new Parser(testCanvas, testSeq);
            string expected = "Command not recognised";

            //Act
            testParser.ParseCommand("notarealcommand 200,200");
            string actual = testSeq.errorlist;

            //Assert
            StringAssert.Contains(actual, expected);
        }
        /// <summary>
        /// Test that if a command has no peramater in it, the peramater string remains null
        /// </summary>
        [TestMethod]
        public void Parser_TestNoPeramater()
        {
            //Arrange
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));
            testParser = new Parser(testCanvas, testSeq);

            //Act
            testParser.ParseCommand("moveto ");
            string actual = testParser.peram;

            //Assert
            Assert.IsNull(actual);
        }
    }
}
