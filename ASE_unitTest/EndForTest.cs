﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_ExamProject;
using System.Drawing;

namespace ASE_unitTest
{
    [TestClass]
    public class EndForTest
    {
        For tfor;
        Endfor tend;
        Sequence tseq;

        /// <summary>
        /// Test that the for loop itteration counter is itterated when the for loop end is ran
        /// </summary>
        [TestMethod]
        public void endForItterTest()
        {
            //Arrange
            tseq = new Sequence();
            tfor = new For(tseq, 1, "10");
            tend = new Endfor(tseq, 2);
            tfor.endLpos = 2;
            tend.startLnum = 1;
            tseq.forList.Add(tfor);
            int expected = 1;

            //Act
            tend.run();
            int actual = tfor.loopCounter;

            //Assert
            Assert.AreEqual(expected, actual);

        }
    }
}
