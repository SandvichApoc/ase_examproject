﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_ExamProject;
using System.Drawing;

namespace ASE_unitTest
{
    [TestClass]
    public class MethodTest
    {
        Sequence tseq;
        Method tmeth;

        /// <summary>
        /// Test to see if the method name can be set correctly
        /// </summary>
        [TestMethod]
        public void MethodSetName()
        {
            //Arrange
            tseq = new Sequence();
            string expected = "tmethod";
            tmeth = new Method(tseq, 1, "tmethod");

            //Act
            string actual = tmeth.methodName;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test to see if a single peramater can be set correctly
        /// </summary>
        [TestMethod]
        public void MethodSetPeram()
        {
            //Arrange
            tseq = new Sequence();
            string expected = "peram1";
            tmeth = new Method(tseq, 1, "tmethod peram1");

            //Act
            string actual = tmeth.methodSequ.varList[0].varName;

            //Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Test to see if a the number of peramaters is recorded correctly
        /// </summary>
        [TestMethod]
        public void MethodSetNumOfPeram()
        {
            //Arrange
            tseq = new Sequence();
            int expected = 2;
            tmeth = new Method(tseq, 1, "tmethod peram1,peram2");

            //Act
            int actual = tmeth.methodPeramNum;

            //Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Test to see if it correctly throws an exception if the user trys to add a method peramater with the same name
        /// </summary>
        [TestMethod]
        public void MethodExceptPeramAlreadyExists()
        {
            //Arrange
            tseq = new Sequence();
            string expected = "Method peramater name already exists";


            //Act
            var e = Assert.ThrowsException<System.ApplicationException>(() => tmeth = new Method(tseq, 1, "tmethod peram1,peram1"));
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
