﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_ExamProject;
using System.Drawing;

namespace ASE_unitTest
{
    [TestClass]
    public class MovetoTest
    {
        MoveTo testMoveto;
        Canvas testCanvas;
        Sequence testSeq = new Sequence();
        Bitmap testBitmap = new Bitmap(400, 400);


        /// <summary>
        /// Test that the parsing of peramaters work for setting the X position
        /// </summary>
        [TestMethod]
        public void Test_ParsePeramatersX()
        {
            //Arrange
            string testInput = ("200,400");
            int expected = 200;
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act
            testMoveto = new MoveTo(testCanvas, testSeq, testInput, 1);
            testMoveto.run();
            int actual = testMoveto.xEndPos;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Test_ParsePeramatersY()
        {
            //Arrange
            string testInput = ("200,400");
            int expected = 400;
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act
            testMoveto = new MoveTo(testCanvas, testSeq, testInput, 1);
            testMoveto.run();
            int actual = testMoveto.yEndPos;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test if the command correctly throws an exception when it isn't given the correct number of peramaters
        /// </summary>
        [TestMethod]
        public void Test_InvalidPeramAmmount_TooLow()
        {
            //Arrange
            string expected = "Incorrect number of peramaters";
            string testInput = "200";
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act
            testMoveto = new MoveTo(testCanvas, testSeq, testInput, 1);
            var e = Assert.ThrowsException<System.ApplicationException>(() => testMoveto.run());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test if the command correctly throws an exception when it isn't given the correct number of peramaters
        /// </summary>
        [TestMethod]
        public void Test_InvalidPeramAmmount_TooHigh()
        {
            //Arrange
            string expected = "Incorrect number of peramaters";
            string testInput = "200,300,400";
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act 
            testMoveto = new MoveTo(testCanvas, testSeq, testInput, 1);
            var e = Assert.ThrowsException<System.ApplicationException>(() => testMoveto.run());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test to see if the command successfully throws a exception when the peramaters it is given are not numbers
        /// </summary>
        [TestMethod]
        public void Test_InvalidPeramAmmount_NotNumber()
        {
            //Arrange
            string expected = "Peramater not a number";
            string testInput = "not,number";
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act 
            testMoveto = new MoveTo(testCanvas, testSeq, testInput, 1);
            var e = Assert.ThrowsException<System.ApplicationException>(() => testMoveto.run());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Test_NullPeram()
        {
            //Arrange
            string expected = "Command requires a peramater";
            string testInput = null;
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act
            testMoveto = new MoveTo(testCanvas, testSeq, testInput, 1);
            var e = Assert.ThrowsException<System.ApplicationException>(() => testMoveto.run());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected,actual);
        }
        /// <summary>
        /// Test to see if the command can parse and then throw an error when the input is negative
        /// </summary>
        [TestMethod]
        public void Test_NegativePeram()
        {
            string expected = "Peramater is negative";
            string testInput = "-200,-400";
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act
            testMoveto = new MoveTo(testCanvas, testSeq, testInput, 1);
            var e = Assert.ThrowsException<System.ApplicationException>(() => testMoveto.run());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
