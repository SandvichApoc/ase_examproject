﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_ExamProject;
using System.Drawing;

namespace ASE_unitTest
{
    [TestClass]
    public class CanvasTest
    {
        Canvas testCanvas;
        Bitmap testBitmap = new Bitmap(400, 400);
        /// <summary>
        /// Test the moveto command moves the pen to the correct Y location
        /// </summary>
        [TestMethod]
        public void Test_MovetoX()
        {
            //Arrange
            int testInputX = 50;
            int testInputY = 100;
            int expected = 50;
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act
            testCanvas.moveto(testInputX, testInputY);
            int actual = testCanvas.getXPos();

            //Assert
            Assert.AreEqual(expected, actual);

        }
        /// <summary>
        /// Test the moveto command moves the pen to the correct Y location
        /// </summary>
        [TestMethod]
        public void Test_MovetoY()
        {
            //Arrange
            int testInputX = 50;
            int testInputY = 100;
            int expected = 100;
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act
            testCanvas.moveto(testInputX, testInputY);
            int actual = testCanvas.getYPos();

            //Assert
            Assert.AreEqual(expected, actual);

        }
        /// <summary>
        /// Test setting the pen colour correctly 
        /// </summary>
        [TestMethod]
        public void Test_SetPen_Pen()
        {
            //Assign
            string testInput = "red";
            Color expected = Color.Red;
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act
            testCanvas.setPen(testInput);
            Color actual = testCanvas.Pen.Color;

            //Assert
            Assert.AreEqual(expected,actual);
        }
        /// <summary>
        /// Test setting the brush colour correctly
        /// </summary>
        [TestMethod]
        public void Test_SetPen_Brush()
        {
            //Assign
            string testInput = "green";
            Color expected = Color.Green;
            testCanvas = new Canvas(Graphics.FromImage(testBitmap));

            //Act
            testCanvas.setPen(testInput);
            Color actual = testCanvas.Brush.Color;

            //Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
