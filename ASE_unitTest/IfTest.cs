﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_ExamProject;
using System.Drawing;

namespace ASE_unitTest
{
    [TestClass]
    public class IfTest
    {
        If testif;
        Sequence Tsequence;
        /// <summary>
        /// assess if it correctly results true in equality if statements
        /// </summary>
        [TestMethod]
        public void Test_EqualityTrue()
        {
            //Arrange
            string testInput = "10 == 10";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results false in equality if statements
        /// </summary>
        [TestMethod]
        public void Test_Equalityfalse()
        {
            //Arrange
            string testInput = "10 == 1";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results true in notequality if statements
        /// </summary>
        [TestMethod]
        public void Test_NotEqualityTrue()
        {
            //Arrange
            string testInput = "10 != 1";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results false in notequality if statements
        /// </summary>
        [TestMethod]
        public void Test_NotEqualityFalse()
        {
            //Arrange
            string testInput = "10 != 10";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results true in biggerthan if statements
        /// </summary>
        [TestMethod]
        public void Test_biggerthanTrue()
        {
            //Arrange
            string testInput = "10 > 5";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results false in biggerthan if statements
        /// </summary>
        [TestMethod]
        public void Test_biggerthanFalse()
        {
            //Arrange
            string testInput = "10 > 10";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results true in lessthan if statements
        /// </summary>
        [TestMethod]
        public void Test_LessThanTrue()
        {
            //Arrange
            string testInput = "10 < 15";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results false in lessthan if statements
        /// </summary>
        [TestMethod]
        public void Test_LessThanFalse()
        {
            //Arrange
            string testInput = "15 < 10";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results true in biggerorequal if statements
        /// </summary>
        [TestMethod]
        public void Test_BigEqualTrue()
        {
            //Arrange
            string testInput = "10 >= 3";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results false in biggerorequal if statements
        /// </summary>
        [TestMethod]
        public void Test_BigEqualFalse()
        {
            //Arrange
            string testInput = "10 >= 15";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results true in lessorequal if statements
        /// </summary>
        [TestMethod]
        public void Test_LessEqualTrue()
        {
            //Arrange
            string testInput = "10 <= 10";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results false in lessorequal if statements
        /// </summary>
        [TestMethod]
        public void Test_LessEqualFalse()
        {
            //Arrange
            string testInput = "15 <= 10";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }

        /// <summary>
        /// assess if an variable can be used within an if statement
        /// </summary>
        [TestMethod]
        public void test_VariableInIF()
        {
            //Arrange
            Tsequence = new Sequence();
            Variable testvar = new Variable("testv", Tsequence,2);
            testvar.setExpression("10");
            Tsequence.varList.Add(testvar);

            string testInput = "testv == 10";
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);

        }
        /// <summary>
        /// assess if a calculation can be done in the if statement
        /// </summary>
        [TestMethod]
        public void Test_calcInStatement()
        {
            //Arrange
            string testInput = "5 + 5 == 10";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }

        /// <summary>
        /// assess if it correctly results in true if the left substatement is true
        /// </summary>
        [TestMethod]
        public void test_ORTrueLeft()
        {
            //Arrange
            string testInput = "(10 == 10) || (5 == 10)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results in true if the right substatement is true
        /// </summary>
        [TestMethod]
        public void test_ORTrueRight()
        {
            //Arrange
            string testInput = "(10 == 5) || (10 == 10)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results in false if the neither substatement is true
        /// </summary>
        [TestMethod]
        public void test_ORfalse()
        {
            //Arrange
            string testInput = "(10 == 15) || (5 == 10)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }

        /// <summary>
        /// assess if it correctly results in true if both statements are true
        /// </summary>
        [TestMethod]
        public void Test_ANDTrue()
        {
            //Arrange
            string testInput = "(10 == 10) && (12 > 5)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results in false if the left statement is false
        /// </summary>
        [TestMethod]
        public void Test_ANDFalseLeft()
        {
            //Arrange
            string testInput = "(17 < 16) && (10 == 10)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results in false if the right statement is false
        /// </summary>
        [TestMethod]
        public void Test_ANDFalseRight()
        {
            //Arrange
            string testInput = "(17 < 18) && (10 == 1)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results in true if the statement is false
        /// </summary>
        [TestMethod]
        public void Test_NotTrue()
        {
            //Arrange
            string testInput = "!(10 == 1000)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsTrue(testif.statementResult);
        }
        /// <summary>
        /// assess if it correctly results in false if the statement is true
        /// </summary>
        [TestMethod]
        public void Test_NotFalse()
        {
            //Arrange
            string testInput = "!(10 == 10)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);
            //Act
            testif.subRun();

            //Assert
            Assert.IsFalse(testif.statementResult);
        }

        /// <summary>
        /// assess if it correctly throws the correct error when there is brackets but no logical operator
        /// </summary>
        [TestMethod]
        public void Test_ExceptBracketsNotNeeded()
        {
            //Arrange
            string testInput = "(17 == 17)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);

            string expected = "If statement should not require brackets if it doesn't have a logical operator";
            //Act
            var e = Assert.ThrowsException<System.ApplicationException>(() => testif.subRun());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Assess if it correctly throws the correct error when there are too many operators
        /// </summary>
        [TestMethod]
        public void Test_ExceptTooManyLogiOper()
        {
            //Arrange
            string testInput = "(17 == 17) && && (10 == 10)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);

            string expected = "Statement has multiple top level logical operators, multiple operators should be within brackets";
            //Act
            var e = Assert.ThrowsException<System.ApplicationException>(() => testif.subRun());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Assess if it correctly throws the correct error when are not enough brackets for an OR statement
        /// </summary>
        [TestMethod]
        public void Test_ExceptNotEnoughStatementOR()
        {
            //Arrange
            string testInput = "(17 == 17) ||";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);

            string expected = "Or operator requires two sections in brackets";
            //Act
            var e = Assert.ThrowsException<System.ApplicationException>(() => testif.subRun());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Assess if it correctly throws the correct error when there are too many brackets for an OR statement
        /// </summary>
        [TestMethod]
        public void Test_ExceptTooManyStatementsOR()
        {
            //Arrange
            string testInput = "(17 == 17) || (10 == 10) (15 >= 31)";
            Tsequence = new Sequence();
            testif = new If(Tsequence, 1, testInput);

            string expected = "If has too many statements";
            //Act
            var e = Assert.ThrowsException<System.ApplicationException>(() => testif.subRun());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }

}
