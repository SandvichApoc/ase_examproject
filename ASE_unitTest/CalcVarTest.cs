﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_ExamProject;
using System.Drawing;

namespace ASE_unitTest
{
    [TestClass]
    public class CalcVarTest
    {
        CalcVar testCalc;
        Variable tVar;
        Sequence Tsequence;
        /// <summary>
        /// Test if a calcvar can set a variable to a number
        /// </summary>
        [TestMethod]
        public void CalcVarExpSet()
        {
            //Arrange
            Tsequence = new Sequence();
            tVar = new Variable("tvar",Tsequence,1);
            Tsequence.varList.Add(tVar);
            testCalc = new CalcVar(Tsequence, 2,"tvar", "= 10");
            string expected = "10";

            //Act
            testCalc.run();
            string actual = tVar.exp;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test if a calcvar can add two numbers
        /// </summary>
        [TestMethod]
        public void CalcVarExpAdd()
        {
            //Arrange
            Tsequence = new Sequence();
            tVar = new Variable("tvar", Tsequence, 1);
            Tsequence.varList.Add(tVar);
            testCalc = new CalcVar(Tsequence, 2, "tvar", "= 10 + 10");
            string expected = "20";

            //Act
            testCalc.run();
            string actual = tVar.exp;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test if a calcvar can minus
        /// </summary>
        [TestMethod]
        public void CalcVarExpMinus()
        {
            //Arrange
            Tsequence = new Sequence();
            tVar = new Variable("tvar", Tsequence, 1);
            Tsequence.varList.Add(tVar);
            testCalc = new CalcVar(Tsequence, 2, "tvar", "= 10 - 5");
            string expected = "5";

            //Act
            testCalc.run();
            string actual = tVar.exp;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test if a calcvar can divide
        /// </summary>
        [TestMethod]
        public void CalcVarExpDivide()
        {
            //Arrange
            Tsequence = new Sequence();
            tVar = new Variable("tvar", Tsequence, 1);
            Tsequence.varList.Add(tVar);
            testCalc = new CalcVar(Tsequence, 2, "tvar", "= 10 / 2");
            string expected = "5";

            //Act
            testCalc.run();
            string actual = tVar.exp;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test if a calcvar can set a variable to a number
        /// </summary>
        [TestMethod]
        public void CalcVarExpMulti()
        {
            //Arrange
            Tsequence = new Sequence();
            tVar = new Variable("tvar", Tsequence, 1);
            Tsequence.varList.Add(tVar);
            testCalc = new CalcVar(Tsequence, 2, "tvar", "= 10 * 3");
            string expected = "30";

            //Act
            testCalc.run();
            string actual = tVar.exp;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test if a calcvar can parse an variable in the expression
        /// </summary>
        [TestMethod]
        public void CalcVarExpVarInExp()
        {
            //Arrange
            Tsequence = new Sequence();
            tVar = new Variable("tvar", Tsequence, 1);
            Variable xVar = new Variable("xvar", Tsequence, 2);
            xVar.exp = "10";
            Tsequence.varList.Add(xVar);
            Tsequence.varList.Add(tVar);
            testCalc = new CalcVar(Tsequence, 3, "tvar", "= 10 + xvar");
            string expected = "20";

            //Act
            testCalc.run();
            string actual = tVar.exp;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test if a calcvar can can be run multiple times as if it were in a loop
        /// </summary>
        [TestMethod]
        public void CalcVarExpMultiRun()
        {
            //Arrange
            Tsequence = new Sequence();
            tVar = new Variable("tvar", Tsequence, 1);
            tVar.exp = "0";
            Tsequence.varList.Add(tVar);
            testCalc = new CalcVar(Tsequence, 2, "tvar", "= tvar + 10");
            string expected = "30";

            //Act
            testCalc.run();
            testCalc.run();
            testCalc.run();
            string actual = tVar.exp;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test if a calcvar can throw an exception if the user didn't use an = at the start
        /// </summary>
        [TestMethod]
        public void CalcVarExpException()
        {
            //Arrange
            Tsequence = new Sequence();
            tVar = new Variable("tvar", Tsequence, 1);
            Tsequence.varList.Add(tVar);
            testCalc = new CalcVar(Tsequence, 2, "tvar", "10");

            string expected = "variable operation unknown";

            //Act
            var e = Assert.ThrowsException<System.ApplicationException>(() => testCalc.run());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
