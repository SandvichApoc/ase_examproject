﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_ExamProject;
using System.Drawing;

namespace ASE_unitTest
{
    [TestClass]
    public class ForTest
    {
        For testfor;
        Sequence tseq;
        /// <summary>
        /// Test that the correct number of itterations can be set
        /// </summary>
        [TestMethod]
        public void TargetIterSetTest()
        {
            // Arrange
            tseq = new Sequence();
            testfor = new For(tseq, 1, "10");
            testfor.endLpos = 2;
            int expected = 10;

            //Act
            testfor.run();
            int actual = testfor.targetIteration;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test that the correct number of itterations can be set using an expression
        /// </summary>
        [TestMethod]
        public void TargetIterSetExpTest()
        {
            // Arrange
            tseq = new Sequence();
            testfor = new For(tseq, 1, "10 + 20");
            testfor.endLpos = 2;
            int expected = 30;

            //Act
            testfor.run();
            int actual = testfor.targetIteration;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test that the correct number of itterations can be set
        /// </summary>
        [TestMethod]
        public void TargetIterSetExpVarTest()
        {
            // Arrange
            tseq = new Sequence();
            Variable tvar = new Variable("tvar", tseq, 1);
            tvar.exp = "10";
            tseq.varList.Add(tvar);
            testfor = new For(tseq, 2, "tvar");
            testfor.endLpos = 3;
            int expected = 10;

            //Act
            testfor.run();
            int actual = testfor.targetIteration;

            //Assert
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Test that the correct exception is thrown when there is no end to the for
        /// </summary>
        [TestMethod]
        public void TargetIterExcptTest()
        {
            // Arrange
            tseq = new Sequence();
            testfor = new For(tseq, 1, "10");
            string expected = "for loop doesn't have an end";

            //Act
            var e = Assert.ThrowsException<System.ApplicationException>(() => testfor.run());
            string actual = e.Message;

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
